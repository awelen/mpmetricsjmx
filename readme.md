# mpMetricsJMX - Register all Microprofile Metrics 2.3 to JMX

Copyright 2018 - 2021 Anders Wel�n, anders@welen.net
(See also "copyright.txt")

Even if one of key features of Microprofile Metrics is to avoid the "complex" JMX infrastructure (See https://github.com/eclipse/microprofile-metrics/blob/master/spec/src/main/asciidoc/intro.adoc) there is a lot of toolkits out there that can collect data through it. This small library will automatically create MBeans under the domain "net.welen.mpMetricsJMX" for all Microprofile Metrics available.

Simple add the jar file to your Microprofile application.
