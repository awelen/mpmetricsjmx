package net.welen.mpMetricsJMX;

import java.util.HashMap;
import java.util.Optional;

import org.eclipse.microprofile.metrics.Metadata;

public abstract class AbstractMetadata {

	private Metadata metadata;
	
	public AbstractMetadata(Metadata metadata) {
		this.metadata = metadata;
	}
	
	public String getDescription() {
		Optional<String> value = metadata.getDescription();

		if (value.isPresent()) {
			return "";
		}
		return value.get();
	}

	public String getDisplayName() {
		return metadata.getDisplayName();
	}

	public String getName() {
		return metadata.getName();
	}

	public String getType() {
		return metadata.getType();
	}

	public String getUnit() {
		Optional<String> value = metadata.getUnit();

                if (value.isPresent()) {
                        return "";
                }
                return value.get();
	}

}
