package net.welen.mpMetricsJMX;

import org.eclipse.microprofile.metrics.Counter;
import org.eclipse.microprofile.metrics.Metadata;

public class CounterWrapper extends AbstractMetadata implements CounterWrapperMBean {

	private Counter counter;
	
	public CounterWrapper(Counter counter, Metadata metadata) {
		super(metadata);
		this.counter = counter;		
	}

	@Override
	public long getCount() {
		return counter.getCount();
	}
	
}
