package net.welen.mpMetricsJMX;

import java.util.HashMap;

public interface CounterWrapperMBean {

	String getDescription();
	String getDisplayName();
	String getName();
	String getType();
	String getUnit();

	long getCount();
}
