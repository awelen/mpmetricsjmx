package net.welen.mpMetricsJMX;

import org.eclipse.microprofile.metrics.Gauge;
import org.eclipse.microprofile.metrics.Metadata;

public class GaugeWrapper extends AbstractMetadata implements GaugeWrapperMBean {

	private Gauge gauge;
	
	public GaugeWrapper(Gauge gauge, Metadata metadata) {
		super(metadata);
		this.gauge = gauge;
	}

	@Override
	public Object getValue() {
		return gauge.getValue();
	}

}
