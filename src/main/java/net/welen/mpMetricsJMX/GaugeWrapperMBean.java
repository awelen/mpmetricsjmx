package net.welen.mpMetricsJMX;

import java.util.HashMap;

public interface GaugeWrapperMBean {

	String getDescription();
	String getDisplayName();
	String getName();
	String getType();
	String getUnit();

	Object getValue();
}
