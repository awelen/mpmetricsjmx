package net.welen.mpMetricsJMX;

import org.eclipse.microprofile.metrics.Histogram;
import org.eclipse.microprofile.metrics.Metadata;

public class HistogramWrapper extends AbstractMetadata implements HistogramWrapperMBean {

	private Histogram histogram;
	
	public HistogramWrapper(Histogram histogram, Metadata metadata) {
		super(metadata);
		this.histogram = histogram;		
	}

	@Override
	public long getCount() {
		return histogram.getCount();
	}
	
}
