package net.welen.mpMetricsJMX;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.context.Destroyed;
import javax.enterprise.event.Observes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.eclipse.microprofile.metrics.Counter;
import org.eclipse.microprofile.metrics.Gauge;
import org.eclipse.microprofile.metrics.Histogram;
import org.eclipse.microprofile.metrics.Metadata;
import org.eclipse.microprofile.metrics.Meter;
import org.eclipse.microprofile.metrics.Metric;
import org.eclipse.microprofile.metrics.MetricRegistry;
import org.eclipse.microprofile.metrics.Timer;
import org.eclipse.microprofile.metrics.annotation.RegistryType;
import org.eclipse.microprofile.metrics.MetricID;
import org.eclipse.microprofile.metrics.Tag;

import java.lang.management.ManagementFactory;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

@ApplicationScoped
public class LifeCycle {
	private final static Logger LOG = Logger.getLogger(LifeCycle.class.getName());
	private final static String DOMAIN = "net.welen.mpMetricsJMX";

	@Inject
	@RegistryType(type = MetricRegistry.Type.BASE)
	private MetricRegistry baseRegistry;

	@Inject
	@RegistryType(type = MetricRegistry.Type.VENDOR)
	private MetricRegistry vendorRegistry;

	@Inject
	@RegistryType(type = MetricRegistry.Type.APPLICATION)
	private MetricRegistry applicationRegistry;

	private MBeanServer server = ManagementFactory.getPlatformMBeanServer();
	private static List<ObjectName> objectNames = new ArrayList<ObjectName>();

	public void setup(@Observes @Initialized(ApplicationScoped.class) Object init) {
		LOG.log(Level.INFO, "Starting mp-metrics-jmx");
		
		for (String scope : new String[]{"base", "vendor", "application"}) {
			MetricRegistry registry = null;			
			switch (scope) {
				case "base":
					registry = baseRegistry;
					break;
				case "vendor":
					registry = vendorRegistry;
					break;
				case "application":
					registry = applicationRegistry;
					break;					
			}
			
			for (Entry<MetricID, Metric> metricEntry : registry.getMetrics().entrySet()) {
				MetricID id = metricEntry.getKey();
				Metric metric = metricEntry.getValue();
				Metadata metadata = registry.getMetadata().get(id.getName());
				
				LOG.log(Level.FINE, "Found metric: " + id.getName() + ":" + metric);				

				StringBuilder tags = new StringBuilder();
				for (Tag tag : id.getTagsAsList()) {
					tags.append("," + tag.getTagName() + "=" + tag.getTagValue());
				}

				ObjectName objectname;
				try {
					objectname = new ObjectName(DOMAIN + ":scope=" + scope + ", name=" + id.getName() + ", type=" + metadata.getType() + tags);
					LOG.log(Level.FINE, "Register MBean: " + objectname);
					
					if (metric instanceof Counter) {
						server.registerMBean(new CounterWrapper((Counter) metric, metadata), objectname);
					} else if (metric instanceof Gauge) {
						server.registerMBean(new GaugeWrapper((Gauge) metric, metadata), objectname);
					} else if (metric instanceof Meter) {
						server.registerMBean(new MeterWrapper((Meter) metric, metadata), objectname);							
					} else if (metric instanceof Histogram) {
						server.registerMBean(new HistogramWrapper((Histogram) metric, metadata), objectname);							
					} else if (metric instanceof Timer) {
						server.registerMBean(new TimerWrapper((Timer) metric, metadata), objectname);							
					} else {
						LOG.log(Level.WARNING, "Unknown Metric: name=" + id.getName() + ", type=" + metadata.getType());
					}
					objectNames.add(objectname);
				} catch (MalformedObjectNameException|InstanceAlreadyExistsException|MBeanRegistrationException|NotCompliantMBeanException e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				}			
			}
		}
		LOG.log(Level.FINE, "Registered " + objectNames.size() + " MBeans");
	}

	public void teardown(@Observes @Destroyed(ApplicationScoped.class) Object init) {
		LOG.log(Level.INFO, "Stopping mp-metrics-jmx");
		
		for (ObjectName objectname : objectNames) {
			if (server.isRegistered(objectname)) {
				try {
					LOG.log(Level.FINE, "Unregister MBean: " + objectname);
					server.unregisterMBean(objectname);
				} catch (MBeanRegistrationException|InstanceNotFoundException e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				}
			}
		}
		LOG.log(Level.FINE, "Unregistered " + objectNames.size() + " MBeans");
	}

}
