package net.welen.mpMetricsJMX;

import org.eclipse.microprofile.metrics.Meter;
import org.eclipse.microprofile.metrics.Metadata;

public class MeterWrapper extends AbstractMetadata implements MeterWrapperMBean {

	private Meter meter;
	
	public MeterWrapper(Meter meter, Metadata metadata) {
		super(metadata);
		this.meter = meter;
	}

	@Override
	public long getCount() {
		return meter.getCount();
	}

	@Override
	public double getOneMinuteRate() {
		return meter.getOneMinuteRate();
	}

	@Override
	public double getFiveMinuteRate() {
		return meter.getFiveMinuteRate();
	}

	@Override
	public double getFifteenMinuteRate() {
		return meter.getFifteenMinuteRate();
	}

	@Override
	public double getMeanRate() {
		return meter.getMeanRate();
	}

}
