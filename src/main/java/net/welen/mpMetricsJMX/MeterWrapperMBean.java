package net.welen.mpMetricsJMX;

import java.util.HashMap;

public interface MeterWrapperMBean {

	String getDescription();
	String getDisplayName();
	String getName();
	String getType();
	String getUnit();

	long getCount();
	double getOneMinuteRate();
	double getFiveMinuteRate();
	double getFifteenMinuteRate();	
	double getMeanRate();	
}
