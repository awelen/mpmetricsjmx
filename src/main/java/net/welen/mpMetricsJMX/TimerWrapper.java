package net.welen.mpMetricsJMX;

import org.eclipse.microprofile.metrics.Metadata;
import org.eclipse.microprofile.metrics.Timer;

public class TimerWrapper extends AbstractMetadata implements TimerWrapperMBean {

	private Timer timer;
	
	public TimerWrapper(Timer timer, Metadata metadata) {
		super(metadata);
		this.timer = timer;		
	}

	@Override
	public long getCount() {
		return timer.getCount();
	}

	@Override
	public double getOneMinuteRate() {
		return timer.getOneMinuteRate();
	}

	@Override
	public double getFiveMinuteRate() {
		return timer.getFiveMinuteRate();
	}

	@Override
	public double getFifteenMinuteRate() {
		return timer.getFifteenMinuteRate();
	}

	@Override
	public double getMeanRate() {
		return timer.getMeanRate();
	}

	@Override
	public double getSnapshot75thPercentile​() {
		return timer.getSnapshot().get75thPercentile();
	}
	
	@Override
	public double getSnapshot95thPercentile​() {
		return timer.getSnapshot().get95thPercentile();
	}

	@Override
	public double getSnapshot98thPercentile​() {
		return timer.getSnapshot().get98thPercentile();
	}

	@Override
	public double getSnapshot999thPercentile​() {
		return timer.getSnapshot().get999thPercentile();
	}

	@Override
	public double getSnapshot99thPercentile​() {
		return timer.getSnapshot().get99thPercentile();
	}

	@Override
	public long getSnapshotMax​() {
		return timer.getSnapshot().getMax();
	}

	@Override
	public double getSnapshotMean​() {
		return timer.getSnapshot().getMean();
	}

	@Override
	public double getSnapshotMedian​() {
		return timer.getSnapshot().getMedian();
	}

	@Override
	public long getSnapshotMin​() {
		return timer.getSnapshot().getMin();
	}

	@Override
	public double getSnapshotStdDev​() {
		return timer.getSnapshot().getStdDev();
	}

	@Override
	public double getSnapshotValue​(double quantile) {
		return timer.getSnapshot().getValue(quantile);
	}

	@Override
	public long[] getSnapshotValues​() {
		return timer.getSnapshot().getValues();
	}

	@Override
	public int getSnapshotSize​() {
		return timer.getSnapshot().size();
	}

}
