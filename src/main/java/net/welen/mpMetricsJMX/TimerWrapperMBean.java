package net.welen.mpMetricsJMX;

import java.util.HashMap;

public interface TimerWrapperMBean {

	String getDescription();
	String getDisplayName();
	String getName();
	String getType();
	String getUnit();

	long getCount();
	double getOneMinuteRate();
	double getFiveMinuteRate();
	double getFifteenMinuteRate();
	double getMeanRate();
	
	double getSnapshot75thPercentile​();
	double getSnapshot95thPercentile​();
	double getSnapshot98thPercentile​();
	double getSnapshot999thPercentile​();
	double getSnapshot99thPercentile​();
	long getSnapshotMax​();
	double getSnapshotMean​();
	double getSnapshotMedian​();
	long getSnapshotMin​();
	double getSnapshotStdDev​();
	double getSnapshotValue​(double quantile);
	long[] getSnapshotValues​();
	int getSnapshotSize​();
}
